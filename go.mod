module acl_koncilia.com/acl/koncilia

go 1.15

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/go-kit/kit v0.10.0
	github.com/gobeam/mongo-go-pagination v0.0.4 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/joho/godotenv v1.3.0
	github.com/pkg/sftp v1.13.0
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/swaggo/http-swagger v1.0.0
	github.com/swaggo/swag v1.7.0
	go.mongodb.org/mongo-driver v1.5.1
	golang.org/x/crypto v0.0.0-20210218145215-b8e89b74b9df
	golang.org/x/text v0.3.5
	gopkg.in/guregu/null.v4 v4.0.0 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
)
