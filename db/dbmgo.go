package db

import (
	"context"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"gopkg.in/mgo.v2/bson"
	"log"
	"sync"
	"time"
)

// DBNAME the name of the DB instance
//const dbName = "matching"

// MongoConnection
type MongoConnection struct {
	Session *mongo.Client
	m		sync.Mutex
}

var BdName = "matching"


//NewConnection
func NewConnection(info string) (mgo *MongoConnection, err error) {

	client, err := mongo.NewClient(options.Client().ApplyURI(info))
	if err != nil {
		log.Fatal(err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	//defer client.Disconnect(ctx)

	if err != nil {
		log.Fatal(err)
	}

	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		panic(err)
	}
	return &MongoConnection{client, sync.Mutex{}}, err
}

// UpdateData
func (con *MongoConnection) UpdateData(collection string, id primitive.ObjectID, ui interface{}) (err error) {

	con.m.Lock()
	defer con.m.Unlock()

	c := con.Session.Database(BdName).Collection(collection)
	idPrimitive, err := primitive.ObjectIDFromHex(id.Hex())
	errs := c.FindOneAndReplace(context.TODO(), bson.M{"_id": idPrimitive}, ui)
	return errs.Err()
}

// UpdateData
func (con *MongoConnection) UpsertData(collection string, id primitive.ObjectID, ui interface{}) (err error) {

	con.m.Lock()
	defer con.m.Unlock()

	c := con.Session.Database(BdName).Collection(collection)
	idPrimitive, err := primitive.ObjectIDFromHex(id.Hex())
	err = c.FindOneAndReplace(context.TODO(), bson.M{"_id": idPrimitive}, ui).Err()

	if err == mongo.ErrNoDocuments{
		ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
		defer cancel()
		_, err = c.InsertOne(ctx, ui)
	}

	return err
}

func (con *MongoConnection) GetFindData(collection string, query bson.M, limit,index int64) (data []bson.M, err error) {

	con.m.Lock()
	defer con.m.Unlock()

	ctx, channel := context.WithTimeout(context.Background(), time.Minute)
	defer channel()

	findOptions := options.Find()
	if limit > 0 {
		findOptions.SetLimit(limit)
		findOptions.SetSkip((index-1)*limit)
		//findOptions.SetSort(bson.D{{"_id", -1}})
	}

	cur, err := con.Session.Database(BdName).Collection(collection).Find(ctx, query, findOptions)

	return con.getData(cur, err, ctx)
}

func (con *MongoConnection) GetFindDataPipe(collection string, query []bson.M, limit,index int64) (data []bson.M, err error) {

	con.m.Lock()
	defer con.m.Unlock()

	ctx, channel := context.WithTimeout(context.Background(), 15*time.Second)
	defer channel()

	pipeline := []bson.M{
		bson.M{"$match": bson.M{"$and": query}}}

	if limit > 0 {
		pipeline = append(pipeline, bson.M{"$skip": (index-1)*limit})
		pipeline = append(pipeline, bson.M{"$limit": limit})
		pipeline = append(pipeline, bson.M{"$sort": bson.M{"time": -1}})
	}

	cur,err := con.Session.Database(BdName).Collection(collection).Aggregate(ctx, pipeline)
	return con.getData(cur, err, ctx)

}

func (con *MongoConnection) getData(cur *mongo.Cursor, err error, ctx context.Context) (data []bson.M, errs error) {

	if err != nil && err != mongo.ErrNoDocuments {
		return data, err
	}

	if err = cur.All(ctx, &data); err != nil {
		return data, err
	}

	defer cur.Close(ctx)

	if err := cur.Err(); err != nil {
		return data, err
	}
	return data, err
}

func (con *MongoConnection) InsertData(collection string, ui interface{}) (err error) {

	con.m.Lock()
	defer con.m.Unlock()
	c := con

	col:= c.Session.Database(BdName).Collection(collection)
	event := ui

	_, err = col.InsertOne(context.TODO(), &event)
	return err
}


func (con *MongoConnection) InsertConciliation(collection string, ui interface{}, ids []string) (err error) {

	con.m.Lock()
	defer con.m.Unlock()
	c := con

	col:= c.Session.Database(BdName).Collection(collection)
	event := ui

	res, err := col.Find(context.TODO(), bson.M{"ids": bson.M{"$in": ids}})

	if !res.Next(context.Background()){
		_, err = col.InsertOne(context.TODO(),&event)
	}
	return err
}