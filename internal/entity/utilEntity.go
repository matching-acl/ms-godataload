package entity

import (
	"encoding/json"
	"strings"
)


func (m *ConfConciliationRequest) KeysJson() ([]KeysJson, error) {
	var jsonManResp []KeysJson
	if err := json.Unmarshal([]byte(m.Keys), &jsonManResp); err != nil {
		println(err)
		return nil, err
	}
	return jsonManResp, nil
}

func (m *ConfConciliationRequest) InterfaceJson() ([]InterfaceJson, error) {
	var jsonManResp []InterfaceJson
	if err := json.Unmarshal([]byte(m.Interfaces), &jsonManResp); err != nil {
		println(err)
		return nil, err
	}
	return jsonManResp, nil
}

func (m *ValidateConciliationRequest) KeysJson() ([]KeysJson, error) {
	var jsonManResp []KeysJson
	if err := json.Unmarshal([]byte(m.Keys), &jsonManResp); err != nil {
		println(err)
		return nil, err
	}
	return jsonManResp, nil
}

func (m *ValidateConciliationRequest) InterfaceJson() []InterfaceJson {
	var jsonManResp []InterfaceJson
	if err := json.Unmarshal([]byte(m.Interfaces), &jsonManResp); err != nil {
		println(err)
		return nil
	}
	return jsonManResp
}

func (m *ValidateConciliationResponse) InterfaceJson() ([]InterfaceJson, error) {
	var jsonManResp []InterfaceJson
	if err := json.Unmarshal([]byte(m.Interfaces), &jsonManResp); err != nil {
		println(err)
		return nil, err
	}
	return jsonManResp, nil
}

func (m *ValidateConciliationResponse) KeysJson() ([]KeysJson, error) {
	var jsonManResp []KeysJson
	if err := json.Unmarshal([]byte(m.Keys), &jsonManResp); err != nil {
		println(err)
		return nil, err
	}
	return jsonManResp, nil
}

func (m *InterfacesRequest) HeaderJson() ([]HeaderJson, error) {
	var jsonManResp []HeaderJson
	if err := json.Unmarshal([]byte(strings.ToUpper(m.Header)), &jsonManResp); err != nil {
		println(err)
		return nil, err
	}
	return jsonManResp, nil
}

func (m *ValidateFileRequest) HeaderJson() ([]HeaderJson, error) {
	var jsonManResp []HeaderJson
	if err := json.Unmarshal([]byte(m.Header), &jsonManResp); err != nil {
		println(err)
		return nil, err
	}
	return jsonManResp, nil
}

