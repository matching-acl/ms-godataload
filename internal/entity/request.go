package entity

import (
	"bytes"
)

type InterfacesRequest struct  {
	Schema string `json:"schema"`
	Code    string `json:"code"`
	Separator string `json:"separator,omitempty"`
	IsTitled bool `json:"is_titled,omitempty"`
	Header string `json:"header"`
}

type ConfConciliationRequest struct {
	Schema string `json:"schema"`
	Code    string `json:"code"`
	Keys       string `json:"keys"`
	Interfaces  string `json:"interfaces"`
}

type ValidateFileRequest struct {
	Separator string `json:"separator,omitempty"`
	Header string `json:"header"`
	Schema string `json:"schema"`
	Model string `json:"model"`
	IsTitled bool `json:"is_titled,omitempty"`
	File bytes.Buffer `json:"file"`
}

type ValidateConciliationRequest struct {
	Keys        string `json:"keys"`
	Interfaces  string `json:"interfaces"`
	File bytes.Buffer  `json:"file"`
	File2 bytes.Buffer `json:"file2"`
	Model       string `json:"model,omitempty"`
}

//Utilities
type InterfaceJson struct {
	InterfaceId int `json:"interfaceId,omitempty"`
	Name string     `json:"name,omitempty"`
	Code string     `json:"code,omitempty"`
	Separator string `json:"separator,omitempty"`
	Header  []HeaderJson `json:"header,omitempty"`
	IsTitled bool `json:"is_titled,omitempty"`
}

type KeysJson []struct {
	Key string `json:"key,omitempty"`
	Type string `json:"type,omitempty"`
	TypeCode string `json:"typeCode,omitempty"`
	Code string `json:"interfaceCode,omitempty"`
	Precision int `json:"precision,omitempty"`
}

//[{'name': 'asd', 'length': '12', 'isEditable': True}]
type HeaderJson struct {
	Name string  `json:"name,omitempty"`
	Length int  `json:"length,omitempty"`
	Position int `json:"position,omitempty"`
	Code string `json:"code,omitempty"`
}




