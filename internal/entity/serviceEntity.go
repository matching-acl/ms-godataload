package entity

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"gopkg.in/mgo.v2/bson"
	"time"

)

// TemplateInterface - Model
type TemplateInterface struct {
	ID    primitive.ObjectID `bson:"_id" json:"_id"`
	Time  time.Time `bson:"time" json:"time"`
	Keys  map[string]string `bson:"keys" json:"keys"`
	Linea string `bson:"linea" json:"linea"`
	File  string `bson:"file" json:"file"`
	Processed int  `bson:"processed" json:"processed"`
}

// Conciliation - Model
type Conciliation struct {
	ID          primitive.ObjectID `bson:"_id" json:"_id"`
	IDs 		[]string  `bson:"ids" json:"ids"`
	Time        time.Time `bson:"time" json:"time"`
	Update      time.Time `bson:"update" json:"update"`
	Interfaces  []InterfaceJson  `bson:"interfaces" json:"interfaces"`
	Keys        []KeysJson  `bson:"keys" json:"keys"`
	Data        map[string][]bson.M  `bson:"data" json:"data"`
	Reprocessed int  `bson:"reprocessed" json:"reprocessed"`
	Conciliated int  `bson:"conciliated" json:"conciliated"`
}

// LogRecord - Model
type LogRecord struct {
	ID            primitive.ObjectID `bson:"_id" json:"_id"`
	Time          time.Time `bson:"time" json:"time"`
	CConciliation string    `bson:"cconciliation" json:"cconciliation,omitempty"`
	TimeRun       time.Time `bson:"timerun" json:"timerun"`
}


