package entity

import "gopkg.in/mgo.v2/bson"

type ValidateConciliationResponse struct {
	Keys            string `json:"keys"`
	Interfaces      string `json:"interfaces"`
	Data            map[int][]TemplateInterface
	Model           string `json:"model,omitempty"`
}


type VConciliationResponse struct {
	Data    []struct {
				Code string `json:"code,omitempty"`
				Name string `json:"name,omitempty"`
				Values []struct{
					Name  string `json:"name,omitempty"`
					Value string `json:"value,omitempty"`
			}
	} `json:"data,omitempty"`
}

type JsonResponse struct {
	Status           string
	StatusCode       int
	Data             JsonInterface
	DataConciliation map[string][]bson.M
	Result           bool
}

//Utilities
type JsonInterface struct {
	Data []TemplateInterface  `json:"Data,omitempty"`
	StatusCode int
	Header []HeaderJson
}

