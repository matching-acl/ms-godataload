package persistance

import (
	"acl_koncilia.com/acl/koncilia/db"
	"acl_koncilia.com/acl/koncilia/internal/entity"
	"bufio"
	"bytes"
	"context"
	"fmt"
	"github.com/pkg/sftp"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"io"
	"os"
	"strings"
	"sync"
	"time"
)

type LoadDataRepository interface {
	ExeLoadInterface(ctx context.Context, r *entity.InterfacesRequest, sftpF *sftp.Client)  error
	ExeValidateFile(ctx context.Context, r *entity.ValidateFileRequest)  (entity.JsonInterface, error)
}

type loadDataRepository struct {
	connMgo *db.MongoConnection
}

func NewLoadDataRepository(connMgo *db.MongoConnection) LoadDataRepository {

	return &loadDataRepository{
		connMgo: connMgo,
	}
}

func (c *loadDataRepository) ExeValidateFile(ctx context.Context, r *entity.ValidateFileRequest) (jsonInterface entity.JsonInterface, err error) {

	scanner := bufio.NewScanner(bytes.NewReader(r.File.Bytes()))

	item := entity.InterfacesRequest{
		Header: r.Header,
		Separator: r.Separator,
		IsTitled: r.IsTitled,
	}

	tempArray, status, err := c.validateFile(scanner, item, r)

	if len(tempArray) == 0 || err != nil{
		status = 400
	}

	header, err := item.HeaderJson()

	if err != nil {
		return jsonInterface, err
	}

	s := entity.HeaderJson{
		Name: "NO ESPECIFICADO",
		Code: "NO ESPECIFICADO",
		Length: 0,
		Position: len(header)+1,
	}

	header = append(header, s)
	jsonInterface = entity.JsonInterface{
		Data:       tempArray,
		StatusCode: status,
		Header: header,
	}

	return jsonInterface, err
}

func (c *loadDataRepository) validateFile (scanner *bufio.Scanner, item entity.InterfacesRequest, r *entity.ValidateFileRequest) (tempArray []entity.TemplateInterface, status int, err error) {

	status = 200

	var intLoop = 0
	var badLine = 0
	var goodLine = 0
	for scanner.Scan() {

		if len(scanner.Text()) <= 1{
			continue
		}

		if item.IsTitled && intLoop == 0 {
			intLoop++
			continue
		}

		if strings.ToUpper(r.Model) == "TEMPLATES" && (intLoop == 3 && !item.IsTitled || intLoop == 4 && item.IsTitled) {
			break
		}

		template, flags, err := c.createTemplateInterfaceController(scanner.Text(), &item, "")

		if err != nil{
			return tempArray, status, err
		}

		if !flags {
			status = 400
			badLine ++
		}else{
			goodLine++
		}

		if (strings.ToUpper(r.Model) == "INTERFACES"  && ((flags && goodLine < 6) || (!flags && badLine < 6))) ||
			(strings.ToUpper(r.Model) != "INTERFACES" && strings.ToUpper(r.Model) != "CONCILIATION_TEST") {
			tempArray = append(tempArray, template)
		}

		if strings.ToUpper(r.Model) == "CONCILIATION_TEST"  && flags {
			tempArray = append(tempArray, template)
		}

		if strings.ToUpper(r.Model) == "INTERFACES" && goodLine == 5 && badLine == 5{
			break
		}

		intLoop++
	}

	return tempArray, status, err

}


func (c *loadDataRepository) ExeLoadInterface(ctx context.Context, r *entity.InterfacesRequest,  sftpF *sftp.Client) (err error) {

	//err2 := godotenv.Load(".env")
	//
	//if err2 != nil {
	//	panic("missing os environment vars.")
	//}

	path := os.Getenv("SFTP_DIR")
	path =  fmt.Sprintf("%s%s/in_%s", path, r.Schema, r.Code)

	c.readFolder(path, r, sftpF)
	return err
}

func (c *loadDataRepository) readFolder(path string, item *entity.InterfacesRequest, sftpF *sftp.Client) {

	var wp sync.WaitGroup

	files, err := sftpF.ReadDir(path)
	fmt.Println("Path " + path)
	if err != nil {
		fmt.Println(fmt.Sprint(err))
	}

	for _, file := range files {
		rutaClean := fmt.Sprintf("%s/%s", path, file.Name())
		rutaMove := fmt.Sprintf("%s/BKP/%s", path, file.Name())
		fmt.Println(rutaClean)

		if !file.IsDir() {
			go func() {
				wp.Add(1)
				defer wp.Done()
				c.readLines(rutaClean, rutaMove, item, sftpF)

			}()
		} else if file.IsDir() && !strings.Contains(file.Name(), "BKP"){
			go func() {
				wp.Add(1)
				defer wp.Done()
				c.readFolder(rutaClean, item, sftpF)
			}()
		}
	}
	wp.Wait()

}

// readLines reads a whole file into memory
// and returns a slice of its lines.
func (c *loadDataRepository) readLines(path, rutaMove string, item *entity.InterfacesRequest, client *sftp.Client) error {

	file, err := client.Open(path)

	if err != nil {
		fmt.Println("Error al leer el fisher "+fmt.Sprint(err))
		return err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)

	intLoop := 0
	for scanner.Scan() {

		if len(scanner.Text()) <= 1{
			continue
		}

		if item.IsTitled && intLoop == 0 {
			intLoop++
			continue
		}

		c.createTemplateInterfaceController(scanner.Text(), item, path)
		intLoop++
	}

	return c.MoveFile (path, rutaMove, client)
}

// CreateTemplateInterfaceController - Create templateInterface
func (c *loadDataRepository) createTemplateInterfaceController(line string, item *entity.InterfacesRequest, path string) (template entity.TemplateInterface, flag bool, err error) {

	template.ID = primitive.NewObjectID()
	template.Time = time.Now()
	template.File = path
	template.Linea = line
	template.Processed = 1

	var m = make(map[string]string)
	var lineSplit []string

	count := 0
	runes := []rune(line)
	header, err := item.HeaderJson()
	lengtH := len(header)

	noEsp := entity.HeaderJson{
		Name: "NO ESPECIFICADO",
		Code: "NO ESPECIFICADO",
		Length: 0,
		Position: 0,
	}

	if item.Separator != ""{
		lineSplit = strings.Split(line, item.Separator)

		if len(lineSplit) == 1{
			return c.badLines(line, item, template, m, noEsp, runes, count, 0)
		}
	}

	for i, s := range header {
		//si existe separador

		if item.Separator != ""{

			//leng d arreglo es mayor que i
			//&& leng del valor de la cadena es menor que la cantidad especificada para esa cadena
			if len(lineSplit)>i && len(lineSplit[i]) <= s.Length && len(lineSplit[i]) > 0 {
				//valid-> valor de la cadena  == length del template
				m[s.Code] = lineSplit[i]
				count += len(lineSplit[i])
			}else{
				//line of transaction invalid
				return c.badLines(line, item, template, m, s, runes, count, i)
			}

		}else {

			if (lengtH-1 == i && count + s.Length > len(line)) || count + s.Length > len(line){
				return c.badLines(line, item, template, m, s, runes, count, i)
			}

			m[s.Code] = string(runes[count:count+s.Length])
			count += s.Length

		}
	}

	if (len(m) == lengtH && count < len(line) && item.Separator == "") ||
		(len(m) == lengtH && len(m) < len(lineSplit) && item.Separator != ""){
		return c.badLines(line, item, template, m, noEsp, runes, count, count)
	}else{
		m["NO ESPECIFICADO"] = "-"
	}

	template.Keys = m

	if item.Code != "" {
		if err := c.connMgo.InsertData(item.Code, template); err != nil {
			return template, true, err
		}
	}

	return template, true, err
}

func (c *loadDataRepository) badLines(line string, item *entity.InterfacesRequest, template entity.TemplateInterface, m map[string]string, s entity.HeaderJson, runes []rune, count int, position int) (result entity.TemplateInterface, flag bool, err error) {

	result = template

	if item.Separator != ""{
		count += len(m)-1
	}
	if count < 0 {
		count = 0
	}else if count > len(line){
		count = len(line)
	}
	result.Processed = 0
	value := string(runes[count:len(line)])

	if strings.HasPrefix(item.Separator, value){
		value = strings.Replace(value, item.Separator, "", 1)
	}

	if _, secondValue := m[s.Code]; !secondValue && value == ""{
		m[s.Code] = "-"
	}else if value != ""{
		m[s.Code] = value
	}

	result.Keys = m

	header, err := item.HeaderJson()

	for _, s := range header {
		if m[s.Code] == ""{
			m[s.Code] = "-"
		}
	}

	if _, secondValue := m["NO ESPECIFICADO"]; !secondValue {
		m["NO ESPECIFICADO"] = "-"
	}

	if item.Code != "" {
		collection := "log_error_" + item.Code
		if err := c.connMgo.InsertData(collection, result); err != nil {
			return result, false, err
		}
	}

	return result, false, err
}

func (c *loadDataRepository) MoveFile(source, destination string, client *sftp.Client) error {

	src, err := client.Open(source)
	if err != nil {
		return err
	}
	defer src.Close()
	dstFile, err := client.Create(destination)
	if err != nil {
		return err
	}
	defer dstFile.Close()

	_, err = io.Copy(dstFile, src)
	if err != nil {
		dstFile.Close()
		client.Remove(destination)
		return err
	}
	err = dstFile.Close()
	if err != nil {
		return err
	}
	err = src.Close()
	if err != nil {
		return err
	}
	err = client.Remove(source)
	if err != nil {
		return err
	}
	println("Move file "+ destination)
	return nil
}
