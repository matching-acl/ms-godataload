package persistance

import (
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"sync"
	"time"

	"acl_koncilia.com/acl/koncilia/db"
	"acl_koncilia.com/acl/koncilia/internal/entity"
	"gopkg.in/mgo.v2/bson"
)


type CConciliationRepository interface {
	ExeCConciliationInterface(ctx context.Context, r *entity.ConfConciliationRequest) error
	ExeValidateCConciliation(response entity.ValidateConciliationResponse) (data map[string][]bson.M, err error, flag bool)
    RunPreConciliation(keyJson []entity.KeysJson, element entity.TemplateInterface, interfaceEval string) (*entity.Conciliation, error)
	GetUpdateLogRecord(collection string, update bool) (timeResponse time.Time, err error)
	RunPreConciliationTest( keyJson []entity.KeysJson, element entity.TemplateInterface, interfaceEval string, elementsTransaction []entity.TemplateInterface) (data map[string][]bson.M, err error)
}

func NewCConciliationRepository(connMgo *db.MongoConnection) CConciliationRepository {
	return &cConciliationRepository{
		connMgo: connMgo,
	}
}

type cConciliationRepository struct {
	connMgo *db.MongoConnection
}


func (c cConciliationRepository) ExeValidateCConciliation(response entity.ValidateConciliationResponse) (data map[string][]bson.M, err error, flag bool) {

	keyJson, err := response.KeysJson()
	dataTransaction := response.Data
	interfaceEval, err := response.InterfaceJson()

	if err != nil {
		return data, err, flag
	}

	data = map[string][]bson.M{}
	for _, interfaceJson := range interfaceEval {
		var bison []bson.M
		bison = append(bison, bson.M{"interface": interfaceJson.Name})
		bison = append(bison, bson.M{"code": interfaceJson.Code})
		bison = append(bison, bson.M{"value": map[string]string{}})
		data[interfaceJson.Code] = bison
	}

	var m = make(map[string]string)
	var d = make(map[string]string)

	conciliated := true
	for _, templateInterface := range dataTransaction[0] {

		for _, keysJson := range keyJson {
			value := ""
			value2 := ""
			for _, s := range keysJson {
				if _, secondValue := templateInterface.Keys[s.Key]; secondValue && value == "" && interfaceEval[0].Code == s.Code{
					value = templateInterface.Keys[s.Key]
					m[s.Key] = value
					data[s.Code][2]["value"] = m
					continue
				}

				for _, t := range dataTransaction[1] {
					if _, secondValue := t.Keys[s.Key]; secondValue && interfaceEval[1].Code == s.Code{
						value2 = t.Keys[s.Key]
						d[s.Key] = value2
						data[s.Code][2]["value"] = d
					}
					if value2 != "" {
						break
					}
				}
				if value2 == "" {
					d[s.Key] = "-"
					data[s.Code][2]["value"] = d
				}
			}

			if value != value2{
				conciliated = false
			}
		}
		if conciliated {
			return data, err, true
		}
	}
	return data, err, false
}

func (c cConciliationRepository) RunPreConciliationTest( keyJson []entity.KeysJson, element entity.TemplateInterface, interfaceEval string, elementsTransaction []entity.TemplateInterface) (data map[string][]bson.M, err error) {

	resultMap := element.Keys

	var m []entity.TemplateInterface
	data = map[string][]bson.M{}
	var queryFilter []bson.M
	for _, keysJson := range keyJson {
		var value string

		for _, s := range keysJson {
			if s.Code == interfaceEval {
				value = resultMap[s.Key]
			}
			if value != ""{
				break
			}
		}

		for _, templateInterface := range elementsTransaction {
			for _, s := range keysJson {

				if s.Code == interfaceEval {
					queryFilter = data[s.Code]

					if len(queryFilter) == 0{
						queryFilter = append(queryFilter, bson.M{s.Key: value})
						data[s.Code] = queryFilter
						continue
					}

					for _, b := range queryFilter {
						if _, secondValue := b[s.Key]; !secondValue{
							queryFilter = append(queryFilter, bson.M{s.Key: value})
							data[s.Code] = queryFilter
							break
						}
					}

					continue
				}

				for s1, s3 := range templateInterface.Keys {

					if s1 == s.Key && s3 == value{
						queryFilter = data[s.Code]

						if len(queryFilter) == 0{
							queryFilter = append(queryFilter, bson.M{s.Key: value})
							data[s.Code] = queryFilter
							m = append(m, templateInterface)
							break
						}

						for _, b := range queryFilter {
							if _, secondValue := b[s1]; !secondValue{
								queryFilter = append(queryFilter, bson.M{s.Key: value})
								data[s.Code] = queryFilter

								m = append(m, templateInterface)
								break
							}
						}
					}
				}
			}
		}
		elementsTransaction = m
	}
	return data, err
}

func (c cConciliationRepository) ExeCConciliationInterface(ctx context.Context, r *entity.ConfConciliationRequest) (err error) {

	interfacesJson, err := r.InterfaceJson()
	keyJson, err := r.KeysJson()

	c.RebuildConciliation(r)

	//devuelve el ultimo registro de la tabla log record para esa conciliacion
	timeLogRecord, _ := c.GetUpdateLogRecord(r.Code, false)

	filter := bson.M{}
	if !timeLogRecord.IsZero() {
		filter = bson.M{"time": bson.M{"$gt": timeLogRecord,
			"$lt": time.Now()}}
	}

	var wp sync.WaitGroup

	for _, interfaceJson := range interfacesJson {

		flag := true
		limit := 10
		page := 1

		for flag{
			//ejecutar la busqueda para todos los registros de interfaces
			element, _ := c.connMgo.GetFindData(interfaceJson.Code, filter, int64(limit), int64(page))

			if element == nil {
				flag = false
				continue
			}
			go func() {
				wp.Add(1)
				defer wp.Done()
				err = c.GoRunPreConciliation(element, keyJson, interfaceJson, interfacesJson, r)
			}()
			page++
		}

	}
	wp.Wait()

	if err == nil || (err != nil && err == mongo.ErrNoDocuments) {
		_, err = c.GetUpdateLogRecord(r.Code, true)
	}
	return err
}

func (c cConciliationRepository) GoRunPreConciliation(element []bson.M, keyJson []entity.KeysJson, interfaceJson entity.InterfaceJson, interfacesJson []entity.InterfaceJson, r *entity.ConfConciliationRequest) (err error) {

	fmt.Println("GoRunPreConciliation ")

	var templateInter []entity.TemplateInterface
	a, _ := json.Marshal(element)
	json.Unmarshal(a, &templateInter)

	for _, elem := range templateInter {

		data, err := c.RunPreConciliation(keyJson, elem, interfaceJson.Code)
		if err != nil {
			return err
		}
		data.Interfaces = interfacesJson
		err = c.connMgo.InsertConciliation(r.Code, data, data.IDs)

	}
	return err
}

func (c cConciliationRepository) RebuildConciliation(r *entity.ConfConciliationRequest) {
	flag := true
	limit := 10
	page := 1

	var wp sync.WaitGroup
	var queryFilter []bson.M
	queryFilter = append(queryFilter, bson.M{"conciliated": 0})
	queryFilter = append(queryFilter, bson.M{"reprocessed": bson.M{"$gte":0, "$lte": 500}})

	for flag {

		d, err := c.connMgo.GetFindDataPipe(r.Code, queryFilter , int64(limit), int64(page))

		if d == nil {
			flag = false
			continue
		}

		go func() {
			wp.Add(1)
			defer wp.Done()
			err = c.GoRebuildConciliation(r.Code, d)
		}()
		page++
	}
	wp.Wait()
}

func (c cConciliationRepository) GoRebuildConciliation(r string, d []bson.M) (err error)  {

	var noConciliated []entity.Conciliation
	a, _ := json.Marshal(d)
	json.Unmarshal(a, &noConciliated)

	if err != nil {
		return err
	}

	if len(noConciliated) > 0 {
		//existen tuplas a las que hay que ejecutarle de nuevo la conciliacion
		for _, i2 := range noConciliated {
			err = c.UpdateConciliation( i2, r)
		}
	}
	return err
}

func (c cConciliationRepository) UpdateConciliation( i2 entity.Conciliation, conciliation string) (err error){
	var updateCon entity.Conciliation
	updateCon = i2

	for s, ms := range i2.Data {
		if len(i2.Keys) == len(ms) {
			found, err := c.connMgo.GetFindDataPipe(s, ms,-1,20)

			if err != nil{
				return err
			}

			if len(found) >= 1 {
				bsM := ms

				var templateInterf []entity.TemplateInterface
				a, _ := json.Marshal(found)
				json.Unmarshal(a, &templateInterf)

				idF := templateInterf[0].ID
				bsM = append(bsM, bson.M{"id": idF})

				updateCon.Data[s] = bsM
				updateCon.IDs = append(updateCon.IDs, idF.String())
				updateCon.Conciliated = 1
				//se encontro una nueva conciliacion
			}
		}
	}
	updateCon.Update = time.Now()
	updateCon.Reprocessed = updateCon.Reprocessed+1

	//if updateCon.Conciliated == 0 && updateCon.Reprocessed == 5{
	//	updateCon.Conciliated = -1
	//}

	err = c.connMgo.UpdateData(conciliation, updateCon.ID, updateCon)
	if err != nil{
		return err
	}

	return err
}

func (c cConciliationRepository) RunPreConciliation( keyJson []entity.KeysJson, element entity.TemplateInterface, interfaceEval string) (data *entity.Conciliation, err error) {

	id := element.ID.String()
	m := map[string][]bson.M{}
	//generar los filtros de la interfaz que estoy evaluando
	//contra la otra interfaz

	//element es la linea de transaccion a la que tengo que buscarle par
	var queryFilter []bson.M
	for _, keysJson := range keyJson {
		var value string
		//si esoty evaluando visa, tengo que buscar en cor
		for _, s := range keysJson {
			if s.Code == interfaceEval {
				value = element.Keys[s.Key]
			}
			if value != ""{
				break
			}
		}

		for _, s := range keysJson {
			queryFilter = m[s.Code]
			queryFilter = append(queryFilter, bson.M{"keys." + s.Key: value})
			m[s.Code] = queryFilter
		}
	}

	result := m
	var ids []string
	flag := 1
	for i,i2 := range m {

		if i == interfaceEval {
			ids = append(ids, id)
			result[i] = append(result[i], bson.M{"id":id})
			continue
		}

		found, _ := c.connMgo.GetFindDataPipe(i, i2,-1, 20)

		if len(found) == 0 {
			flag = 0
			continue
		}
		var tem []entity.TemplateInterface
		a, _ := json.Marshal(found)
		json.Unmarshal(a, &tem)

		idF := tem[0].ID //.(primitive.D).Map()["_id"].(primitive.ObjectID).String()
		result[i] = append(result[i], bson.M{"id":idF.String()})
		ids = append(ids, idF.String())

	}

	data = &entity.Conciliation{
		ID:     primitive.NewObjectID(),
		IDs:    ids,
		Time:   time.Now(),
		Update: time.Now(),
		Keys:   keyJson,
		Data:   result,
		Conciliated: flag,
	}
	return data, err
}

func (c cConciliationRepository) GetUpdateLogRecord(conciliation string, update bool) (timeResponse time.Time, err error) {

	item2, err := c.connMgo.GetFindData("log_record", bson.M{"cconciliation": conciliation},1,1)
	var logRecord []entity.LogRecord

	if item2 != nil{
		a, _ := json.Marshal(item2)
		json.Unmarshal(a, &logRecord)
	}

	if update {
 		ids    := primitive.NewObjectID()
		timeId := time.Now()
		if item2 != nil{
			ids = logRecord[0].ID
			timeId = logRecord[0].TimeRun
		}
		newLogRecord := &entity.LogRecord{
			ID:            ids,
			Time:          timeId,
			TimeRun:       time.Now(),
			CConciliation: conciliation,
		}
		err = c.connMgo.UpsertData("log_record", ids, newLogRecord)
	}

	if item2 != nil {
		timeResponse = logRecord[0].Time
	}
	return timeResponse, err
}

func (c cConciliationRepository) GenerateKeys(code string, interfaces []entity.InterfaceJson) []string {

	var interfaceCode []string

	for _, inf := range interfaces {
		for _, inf2 := range interfaces {
			if inf.Code != inf2.Code{
				interfaceCode = append(interfaceCode, code+"__"+inf.Code+"__"+inf2.Code)
			}
		}
	}
	return interfaceCode
}