package middleware

import (
	"acl_koncilia.com/acl/koncilia/db"
	"acl_koncilia.com/acl/koncilia/internal/entity"
	"acl_koncilia.com/acl/koncilia/internal/persistance"
	"acl_koncilia.com/acl/koncilia/internal/service"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/gorilla/mux"
	"github.com/pkg/sftp"
	httpSwagger "github.com/swaggo/http-swagger"
	"net/http"
	"os"
	"strconv"
)

type App struct {
	Router *mux.Router
	DB     *db.MongoConnection
	Logg   log.Logger
	LoadRepository persistance.LoadDataRepository
	CConciliationRepository persistance.CConciliationRepository
	Sftp *sftp.Client
}

func (a *App) Run(addr string) error{
	err := http.ListenAndServe(addr, a.Router)
	return err
}

// CORS Middleware
func CORS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Set headers
		w.Header().Set("Access-Control-Allow-Headers:", "*")
		w.Header().Set("Access-Control-Allow-Origin", os.Getenv("ORIGIN_ALLOWED"))
		w.Header().Set("Access-Control-Allow-Methods", "*")

		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}

		next.ServeHTTP(w, r)
		return
	})
}

func (a *App) Initialize() (err error) {
	fmt.Println("Starting the application....")

	//err = godotenv.Load(".env")
	//
	//if err != nil {
	//	panic("missing os environment vars.")
	//}

	host :=  os.Getenv("MONGO_HOST")
	_user :=  os.Getenv("MONGO_USER")
	_password :=  os.Getenv("MONGO_PASS")

	con :=  fmt.Sprintf("mongodb+srv://%s:%s@%s/admin?ssl=true&retryWrites=true", _user,_password,host)
	
	a.DB, err = db.NewConnection(con)
	if err != nil {
		println(err.Error())
		return err
	}
	fmt.Println("Connected to MongoDB!")

	mux:= mux.NewRouter()
	mux.Use(CORS)

	a.Router = mux
	a.initializeRoutes()
	a.initializeLogger()
	a.initializeRepository()
	err = a.initializeSftp()

return err

}

// routing
func (a *App) initializeRoutes() {

	a.Router.HandleFunc("/api", httpSwagger.WrapHandler)
	a.Router.Handle("/index", RecoverWrap(http.HandlerFunc(a.getIndex))).Methods("GET")
	a.Router.Handle("/load_data", RecoverWrap(http.HandlerFunc(a.exeLoadData))).Methods("POST")
	a.Router.Handle("/validate_file", RecoverWrap(http.HandlerFunc(a.exeValidateFile))).Methods("POST")
	a.Router.Handle("/c_conciliation", RecoverWrap(http.HandlerFunc(a.exeConfConciliation))).Methods("POST")
	a.Router.Handle("/validate_conciliation", RecoverWrap(http.HandlerFunc(a.exeValidateConciliation))).Methods("POST")

}

func RecoverWrap(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			r := recover()
			if r != nil {
				var err error
				switch t := r.(type) {
				case string:
					err = errors.New(t)
				case error:
					err = t
				default:
					err = errors.New("Unknown error")
				}
				respondWithError(w, http.StatusInternalServerError, err.Error())
			}
		}()
		h.ServeHTTP(w, r)
	})
}

// Logger
func (a *App) initializeLogger() {
	var logger log.Logger
	{
		logger = log.NewLogfmtLogger(os.Stderr)
		logger = log.With(logger, "ts", log.DefaultTimestampUTC)
		logger = log.With(logger, "caller", log.DefaultCaller)
	}
	a.Logg = logger
}

// Repository
func (a *App) initializeRepository() {

	a.LoadRepository = persistance.NewLoadDataRepository(a.DB)
	a.CConciliationRepository = persistance.NewCConciliationRepository(a.DB)
}

// Repository
func (a *App) initializeSftp() error {


	host :=  os.Getenv("SFTP_HOST")
	user :=  os.Getenv("SFTP_USER")
	password :=  os.Getenv("SFTP_PASSWORD")
	port, err := strconv.Atoi(os.Getenv("SFTP_PORT"))


	//err := godotenv.Load(".env")
	//
	//if err != nil {
	//	panic("missing os environment vars.")
	//}

	if err != nil{
		return err
	}

	sftp, err := service.NewConnSftp(host,user, password, port)

	if err != nil{
		println(err.Error())
		return err
	}
	a.Sftp = sftp.Client
	return err
}


func (a *App) getIndex(w http.ResponseWriter, r *http.Request) {
	respondWithJSON(w, http.StatusOK, &entity.JsonResponse{})
}

// helpers
func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, &entity.JsonResponse{Status: message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload *entity.JsonResponse) {

	payload.StatusCode = code
	if payload.Status == ""{
		payload.Status = http.StatusText(code)
	}
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}