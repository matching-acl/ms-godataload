package middleware

import (
	"acl_koncilia.com/acl/koncilia/db"
	"acl_koncilia.com/acl/koncilia/internal/entity"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
)

func (a *App) exeLoadData(w http.ResponseWriter, r *http.Request) {

	if (*r).Method == "OPTIONS" {
		respondWithError(w, http.StatusInternalServerError,"Error en la peticion")
		return
	}

	request := &entity.InterfacesRequest{}

	defer func(begin time.Time) {
		a.Logg.Log("method", "LoadData", "took", time.Since(begin))
	}(time.Now())

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	db.BdName = request.Schema
	defer r.Body.Close()

	println("request" + request.Code , request.Header , request.Schema)

	path := os.Getenv("SFTP_DIR")
	path =  fmt.Sprintf("%s%s/in_%s", path, request.Schema, request.Code)

	_, err := a.Sftp.ReadDir(path)

	if err != nil && err.Error() == "Connection Lost" {
	  	a.initializeSftp()
		println("Reiniciando coneccion sftp")
	}

	err = a.LoadRepository.ExeLoadInterface(r.Context(), request, a.Sftp)


	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJSON(w, http.StatusCreated, &entity.JsonResponse{})
}

func (a *App) exeValidateFile(w http.ResponseWriter, r *http.Request) {

	if (*r).Method == "OPTIONS" {
		respondWithError(w, http.StatusInternalServerError, "Error en la peticion")
		return
	}

	//if r.ContentLength > 1024 {
	//	http.Error(w, "Request too large", http.StatusExpectationFailed)
	//	return
	//}

	//r.Body = http.MaxBytesReader(w, r.Body, 1024)
	//err := r.ParseMultipartForm(1024)

	request := &entity.ValidateFileRequest{}

	defer func(begin time.Time) {
		a.Logg.Log("method", "ValidateFile", "took", time.Since(begin))
	}(time.Now())

	file, handler, err := r.FormFile("file")
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}
	defer file.Close()

	f, err := os.OpenFile(handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}
	defer f.Close()

	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, file); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	request.File = *buf
	request.Separator = r.FormValue("separator")
	request.Header = r.FormValue("header")
	request.Schema = r.FormValue("schema")
	request.Model = r.FormValue("model")
	request.IsTitled, _ = strconv.ParseBool(r.FormValue("is_titled"))

	db.BdName = request.Schema

	defer r.Body.Close()

	var response entity.JsonInterface
	response,err = a.LoadRepository.ExeValidateFile(r.Context(), request)

	if err != nil {
		fmt.Println(err.Error())
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondWithJSON(w, http.StatusCreated, &entity.JsonResponse{Data: response})
}