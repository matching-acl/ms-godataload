package middleware

import (
	"acl_koncilia.com/acl/koncilia/db"
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"os"
	"time"

	"acl_koncilia.com/acl/koncilia/internal/entity"
)

func (a *App) exeValidateConciliation(w http.ResponseWriter, r *http.Request) {

	if (*r).Method == "OPTIONS" {
		respondWithError(w, http.StatusInternalServerError, "Error en la peticion")
		return
	}

	defer func(begin time.Time) {
		a.Logg.Log("method", "exeValidateConciliation", "took", time.Since(begin))
	}(time.Now())

	request := &entity.ValidateConciliationRequest{}

	err, buf := a.defFile(w, r, "file")
	err, buf2 := a.defFile(w, r, "file2")

	request.File = *buf
	request.File2 = *buf2

	request.Interfaces = r.FormValue("interfaces")
	request.Keys = r.FormValue("keys")

	defer r.Body.Close()

	err, arrInter := a.evFile(*request)

	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return
	}

	var response entity.ValidateConciliationResponse

	response.Keys = request.Keys
	response.Interfaces = request.Interfaces

	response.Data = map[int][]entity.TemplateInterface{}
	for i, fileRequest := range arrInter {
		lineTrans, err := a.LoadRepository.ExeValidateFile(r.Context(), fileRequest)
		if len(lineTrans.Data) == 0{
			respondWithError(w, http.StatusNotAcceptable, "La interfaces, no tienen registros válidos para la conciliación.")
			return
		}
		if err != nil {
			respondWithError(w, http.StatusBadRequest, err.Error())
			return
		}
		response.Data[i] =  lineTrans.Data
	}

	response2, err, flag := a.CConciliationRepository.ExeValidateCConciliation(response)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusCreated, &entity.JsonResponse{DataConciliation: response2,Result: flag})
}

func (a *App) defFile(w http.ResponseWriter, r *http.Request, fileName string) (error, *bytes.Buffer) {
	file, handler, err := r.FormFile(fileName)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return err, nil
	}
	defer file.Close()

	f, err := os.OpenFile(handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return err, nil
	}
	defer f.Close()

	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, file); err != nil {
		respondWithError(w, http.StatusBadRequest, err.Error())
		return err, nil
	}
	return err, buf
}

func (a *App) evFile(r entity.ValidateConciliationRequest) ( err error, arrayInterface []*entity.ValidateFileRequest) {

	interfacesJson := r.InterfaceJson()

	for i, conciliationJson := range interfacesJson {

		headerString, err := json.Marshal(conciliationJson.Header)

		if err != nil {
			return err, nil
		}

		interfaceOne := &entity.ValidateFileRequest{
			Model:     "CONCILIATION_TEST",
			Schema:    "",
			Header:    string(headerString),
			Separator: conciliationJson.Separator,
			IsTitled:  conciliationJson.IsTitled,
		}
		if i == 0 {
			interfaceOne.File = r.File
		} else {
			interfaceOne.File = r.File2
		}
		arrayInterface = append(arrayInterface, interfaceOne)
	}

	return err, arrayInterface
}


func (a *App) exeConfConciliation(w http.ResponseWriter, r *http.Request) {

	if (*r).Method == "OPTIONS" {
		respondWithError(w, http.StatusInternalServerError, "Error en la peticion")
		return
	}

	defer func(begin time.Time) {
		a.Logg.Log("method", "exeConfConciliation", "took", time.Since(begin))
	}(time.Now())

	request := &entity.ConfConciliationRequest{}

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&request); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid request payload")
		return
	}

	db.BdName = request.Schema
	defer r.Body.Close()

	err := a.CConciliationRepository.ExeCConciliationInterface(r.Context(), request)

	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondWithJSON(w, http.StatusCreated, &entity.JsonResponse{})
}