FROM golang:1.15.6 AS builder

# ENV GO111MODULE=on \
#     CGO_ENABLED=0 \
#     GOOS=linux \
#     GOARCH=amd64

WORKDIR /build

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN GOOS=linux GO111MODULE=on CGO_ENABLED=0 go build -o main cmd/main.go
# RUN go build -o main main.go

WORKDIR /dist

RUN cp /build/main .

FROM alpine

COPY --from=builder /dist/main /opt/acl-concilia/cmd/

ENTRYPOINT [ "/opt/acl-concilia/cmd/main" ]