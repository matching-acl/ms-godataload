package main

import (
	"acl_koncilia.com/acl/koncilia/cmd/docs"
	"acl_koncilia.com/acl/koncilia/internal/middleware"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	fmt.Println("Start process -->   ")

	docs.SwaggerInfo.Title = "Api Ms GoDataLoad"
	docs.SwaggerInfo.Version = "1.0"

	errors := make(chan error)

	addr := ":9090"

	httpHandler := middleware.App{}
	httpHandler.Initialize()

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM)
	httpHandler.Run(addr)
	errors <- fmt.Errorf("%s", <-ch)

	//go func() {
	//	errors <- httpHandler.Run(addr)
	//}()


}





